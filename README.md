# coin-chart

A simple board displaying crypto charts at command line.
Bitcoin is the default coin.

## Install

```bash
$ npm install -g coin-chart
```

## Usage

Please make sure `bitcoin-chart-cli` is installed first

```bash
$ npm install -g bitcoin-chart-cli
```

### 90 Days analysis

```bash
$ coin
```

<p align="center">
<img src="images/demo-coin.png" width="600">
</p>

### Year analysis

```bash
$ coin --all
```

<p align="center">
<img src="images/demo-coin-all.png" width="600">
</p>

### Other coins

```bash
$ coin etherum
```

## Test

Run `npm test`

## Changelog

[Changelog](./CHANGELOG.md)

## License

[MIT](./LICENSE)
