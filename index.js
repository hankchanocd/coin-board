#!/usr/bin/env node --harmony

"use strict";

/**
 * coin-board displays a board of crypto charts at command line
 */

// Dependencies
const chalk = require("chalk");
const size = require("window-size");
const ui = require("cliui")();

// child_process
const util = require("util");
const execChildProcess = require("child_process").exec;
const spawn = require("child_process").spawn;
const exec = util.promisify(execChildProcess);

// --all
if (
	process.argv[2] &&
	(process.argv[2] == "-a" || process.argv[2] == "--all")
) {
	// Utilize the asynchronous power of Javascript
	Promise.all([
			exec("bitcoin-chart-cli --hours=24 -w 50 -h 10"),
			exec("bitcoin-chart-cli --days=7 -w 50 -h 10"),
			exec("bitcoin-chart-cli --days=30 -w 50 -h 10"),
			exec("bitcoin-chart-cli -w 50 -h 10"),
			exec("bitcoin-chart-cli --days=180 -w 50 -h 10"),
			exec("bitcoin-chart-cli --days=360 -w 50 -h 10")
		])
		.then(result => {
			let [{
					stdout: chartOne
				},
				{
					stdout: chartTwo
				},
				{
					stdout: chartThree
				},
				{
					stdout: chartFour
				},
				{
					stdout: chartFive
				}, {
					stdout: chartSix
				}
			] = result;

			let template = function (chart) {
				return {
					text: chart,
					width: 60,
					padding: [0, 0, 0, 0]
				};
			};

			if (size.width > 120) {
				// If not `less` and have enough width
				ui.div(template(chalk.blueBright(chartOne)), template(chartTwo));
				ui.div(template(chartThree), template(chartFour));
				ui.div(template(chartFive), template(chartSix));
			} else {
				ui.div(template(chalk.blueBright(chartOne)));
				ui.div(template(chartTwo));
				ui.div(template(chartThree));
				ui.div(template(chartFour));
				ui.div(template(chartFive));
				ui.div(template(chartSix));
			}

			return spawn(`cat <<< "${ui.toString()}" | less -r`, {
				stdio: 'inherit',
				shell: true
			});
		})
		.catch(err => console.log(chalk.redBright(err)));


} else {
	// The default

	if (size.width > 90) {
		// If not `less` and have enough width
		exec("bitcoin-chart-cli --width=90")
			.then(result => {
				let {
					stdout
				} = result;
				return spawn(`cat <<< "${chalk.blueBright(stdout)}" | less -r`, {
					stdio: 'inherit',
					shell: true
				});
			})
			.catch(err => console.log(chalk.redBright(err)));

	} else {
		exec("bitcoin-chart-cli --width=50")
			.then(result => {
				let {
					stdout
				} = result;
				return spawn(`cat <<< "${chalk.blueBright(stdout)}" | less -r`, {
					stdio: 'inherit',
					shell: true
				});
			})
			.catch(err => console.log(chalk.redBright(err)));

	}
}
