# Changelog

**2018-Oct-26:** Release v1. Use `bitcoin-chart-cli` only temporarily.

**2018-Oct-27:** Use `semantic-release-gitlab`, but not publish to NPM yet. Use [`npm-publish-git-tag`](https://www.npmjs.com/package/npm-publish-git-tag) for publishing.

**2018-Nov-18:** Add charts to collections
